

# A Game of Battleships

implementation of terminal-based, Player vs. AI battleships using the C++ language.



 
## setup  

 The program requires three types of configuration files placed in the data directory in order to work properly.    

  - config.cfg

  - preset`<number>`.stp

  - ai_config.cfg



### - config.cfg  

  Contains informaton about numbers and sizes of ships present in game.  
  Addictionaly provides the number of prepared presets for AI ships placement.   

  example:

  ```
#ShipConfig
4 3 3 2 2 2
#NumberOfAiPresets
4
  ```  
  In this example game will feature:

  - `1 x 4field ship`
  - `2 x 3field ship`
  - `3 x 2field ship`
  
  And also   `4` presets that are determined in the second type of config files  

### - preset`<number>`.stp

Contains preset determining location of enemy ships.  
There should always be as many files as `#NumberOfAiPresets` in file `config.cvg` specifies. Addictionaly those files need to be named `preset1.stp`,`preset2.stp`,`preset3.stp`....and so on respectively.     
At the beginning of the program, a number from a scpoe (1-`#NumberOfAiPresets`) will be selected, and a file with the matching number will load its configuration to the game.



example:

  ```
#head  
#ships  
E3 0
B2 1
J1 1
A10 0
D7 1
G8 0
#end  
  ```

  Uppercaps letter and a number are the coordinates of top-left tile of a given ship, and the bool value decides if coresopnding ship is placed horizontaly or verticaly.  
  

  `0=horizontal`

  `1=vertical`
  
  
  
### - ai_config.cfg

Contains variables for AI fine-tuning. Basic configuration is well tuned, changing it may destabilise AI

```
#aiMultipliers
20 #hit
1.02 #possible
0.99 #impossible
#end
```
Presented variables are used in evaluating  a  'weight' of a filed (how propable is it for a field to have a ship.
Every field on a 10x10 grid has a weight value.

- `hit` value specifies how big weight wil be set for neighbors of field where ai succefuly targeted a ship

- `possible` is a weight multiplier that the program will apply on a field it sees as possibly a ship

- `impossible` is a weight multiplier that the program will apply on a field it doesn't see as possibly a ship

## Adding new presets
  A user can add his own presets, but the configuration must be properly created

  - value under `#NumberOfAiPresets` in `config.cfg` must always  be the number of `preset<num>.stp` files
  - In `preset<num>.stp` number of ships must equal  number of ships specified in `config.cfg ` file
  - Ships in `preset<num>.stp` file mustn't violate the rules of battleships game, meaning that they must not touch witch sides nor with the corners



## Instructions


At the start of a game, player gets to place his ships in order from longest to shortest ships.  

Player inputs a combination of character (`A-J`) and a number (`1-10)`. In the specified filed, a ship's top-left tille will be created. 
After that, player chooses horizontal or vertical orientation by inputing a character (`V`-vertical,`H`-horizontal).  
After placing all ships, turn-based combat starts. Each turn, player inputs a combination of character(`A-J`) and number (`1-10`) to specify a field to shoot at. Then the same is done by the enemy played by ai.  
The game ends when player or his enemy loses all ships.    
### Galery
![cpp](/assets/gamestart.png)
![cpp](/assets/shooting.png)
![cpp](/assets/gameend.png)

## Division of funcionalities
  Program is divided in 5 main components  

| component | Description | Main author |
| --- | ----------- | ----------- |
| `game` | Connects all the rest and is responsible for a core gameplay | Bartłomiej Gala |
| `field` | Is a building block of a game board | Bartłomiej Gala |
| `ships` | is a class containing info about ships on the board | Piotr Dawid |
| `graphics` | Contains interface functions | Piotr Dawid  |
| `ai` | Decides the moves of ai enemy | Piotr Dawid, Bartłomiej Gala |

### work divison outside of core mechanics

| task peformed | person responsible |
| --- | ----------- |
| creating documentation | Piotr Dawid |
| testing and additional bugfixing | Bartłomiej Gala |




## Tehnologies and tools  used

[![cpp](/assets/cpp.png)](https://en.wikipedia.org/wiki/C%2B%2B)
[![vsc](/assets/vsc.png)](https://en.wikipedia.org/wiki/Visual_Studio_Code)
[![git](/assets/git.png)](https://en.wikipedia.org/wiki/Git )
[![repo](/assets/bitbucket.png)](https://en.wikipedia.org/wiki/Bitbucket )


## Authors

- Piotr Dawid
- Bartłomiej Gala



