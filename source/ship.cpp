#include "../headers/ship.h"


Ship::Ship(unsigned _length)
{
    this->ship_length = _length;
    this->hp = _length;
}

Ship::~Ship()
{


}

bool Ship::isSunk()
{
    if (this->hp == 0)
        return true;
    else
        return false;
}
void Ship::hit()
{
    this->hp--;
    isSunk();
}

unsigned Ship::getLength()
{
    return this->ship_length;
}