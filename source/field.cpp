#include "../headers/field.h"

Field::Field()
{
    ship = nullptr;
    hit = false;
}

void Field::setShip(Ship *newShip)
{
    ship = newShip;
}

bool Field::isHit()
{
    return hit;
}

void Field::hitShip()
{
    if (isShip())
    {
        hit = true;
        ship->hit();
    }
    else
    {      
        hit = true;        
    } 
}

bool Field::isShip()
{
    if (ship != nullptr)
        return true;
    return false;
}

Ship* Field::getShip()
{
    return ship;
}
