#include "../headers/game.h"

Game::Game()
{    
    try
    {
        loadConfigFromFile();
    } 
    catch(const std::string e)
    {
        Graphics::putString(e);
        exit(EXIT_FAILURE);
    }
       
    gameStatus = false;
    turn = 0;
    for(int i = 0; i < shipSizes.size(); i++)    
    {
        p1Ships.push_back(Ship(shipSizes[i]));
        p2Ships.push_back(Ship(shipSizes[i]));
    }    

    p1ShipNumber = shipSizes.size();
    p2ShipNumber = shipSizes.size();
}

// Starts game
void Game::start()
{
    gameStatus = true;
    Ai enemy(shipSizes);

    try
    {
        loadPlacingFromFile(1);
    }
    catch(const std::string e)
    {
        Graphics::putString(e);
        exit(EXIT_FAILURE);
    }  

    playerSetShip();  

    bool turnRepeat = false;
    std::string turnRepeatMessage = "";

    //Main game loop
    while(gameStatus)
    {
        int player = turn%2;       

        if(player == 0)
        {
            Graphics::clearBoard();
            Graphics::drawPlayerBoard(*this);
            std::cout << "Player's ships left: " << p1ShipNumber << std::endl;
            Graphics::drawEnemyBoard(*this);
            std::cout << "Enemy ships left: " << p2ShipNumber << std::endl;
            
            // Inform player that he is repeating turn
            if(turnRepeat)
            {
                Graphics::putString(turnRepeatMessage);
                turnRepeat = false;
            }

            Graphics::putString("Your Turn:");
            std::vector<int> target;
            Graphics::getInput(*this,0,target);

            // Check for correct input
            if(!checkIfInBoard(target))
            {
                turnRepeatMessage = "Coordinates outside the board - try again";
                turnRepeat = true;
                continue;
            }
            if(p2Board[target[0]][target[1]].isHit())
            {
                turnRepeatMessage = "This location has been already hit - try again";
                turnRepeat = true;
                continue;
            }          

            hitShip(target,player);

            //Check if ship is hit
            if(p2Board[target[0]][target[1]].isShip() && p2Board[target[0]][target[1]].getShip()->isSunk())
            {
                p2ShipNumber--;
            }
            turn++;
        }
        else // Ai
        {
            std::vector<int> target = enemy.returnCoOrdinates();
            hitShip(target,player);

            bool isShip = p1Board[target[0]][target[1]].isShip();
            bool isSunk = false;
            if(isShip)
            {
                isSunk = p1Board[target[0]][target[1]].getShip()->isSunk();
            }

            if(isSunk)
            {
                p1ShipNumber--;
            }

            enemy.targetModify(target[0], target[1], isShip, isSunk);
            turn++;
        }

        if(p1ShipNumber == 0 || p2ShipNumber == 0)
        {
            gameStatus = false;
        }        
    }

    result();
}

// Displays game result
void Game::result()
{
    Graphics::clearBoard();
    Graphics::putString("Game Results:");
    Graphics::drawPlayerBoard(*this);
    Graphics::postGameDrawEnemyBoard(*this);

    if(turn%2)
    {
        Graphics::putString("You won!");
    }
    else
    {
        Graphics::putString("Computer won!");
    }

    std::cout << "Number of turns: " << turn/2 << std::endl;
}

// Places ship on player1 board
void Game::p1PlaceShip(std::vector<int>& placement, bool vertical, Ship &ship)
{
    for(int i = 0; i < ship.getLength(); i++)
    {
        if(!vertical)
        {
            p1Board[placement[0]][placement[1]+i].setShip(&ship);
        }
        else
        {
            p1Board[placement[0]+i][placement[1]].setShip(&ship);
        }
    }
}

// Places ship on player2 board
void Game::p2PlaceShip(std::vector<int>& placement, bool vertical, Ship &ship)
{
    for(int i = 0; i < ship.getLength(); i++)
    {
        if(!vertical)
        {
            p2Board[placement[0]][placement[1]+i].setShip(&ship);
        }
        else
        {
            p2Board[placement[0]+i][placement[1]].setShip(&ship);
        }
    }
}

// Checks if given coordinates dont exceede board limits (std::vector<int>& placement)
bool Game::checkIfInBoard(std::vector<int>& placement)
{
    return !(placement[0] > 9 || placement[1] > 9 || placement[0] < 0 || placement[1] < 0);
}

// Checks if given coordinates dont exceede board limits (x and y)
bool Game::checkIfInBoard(int y, int x)
{
    return !(y > 9 || x > 9 || y < 0 || x < 0);
}

// Checks is placement possible for Player1
bool Game::checkP1Placement(std::vector<int>& placement, bool vertical, Ship &ship)
{   
    // Board limits
    if(!checkIfInBoard(placement)) 
    {
        Graphics::putString("Invalid placement - exceeds board - try again\n");
        return false;
    } 

    // checks if ship fits
    if(vertical)
    {        
        if(placement[0]+ship.getLength()>10) // Check borders
        {
            Graphics::putString("Invalid placement - exceeds board - try again\n");
            return false;
        }
        for(int i = -1; i <= (int)ship.getLength(); i++) // Check overlap
        {
            for(int j = -1; j < 2; j++)
            {
                if(!checkIfInBoard(placement[0]+i, placement[1]+j)) continue;
                if(p1Board[placement[0]+i][placement[1]+j].isShip())
                {
                    Graphics::putString("Invalid placement - Ship overlaps - try again\n");
                    return false;
                }
            }            
        }  
    }
    else
    {
        if(placement[1]+ship.getLength()>10) // Check borders
        {
            Graphics::putString("Invalid placement - exceeds board - try again\n"); 
            return false;
        }
        for(int i = -1; i <= (int)ship.getLength(); i++) // Check overlap
        {
           for(int j = -1; j < 2; j++)
            {
                if(!checkIfInBoard(placement[0]+j, placement[1]+i)) continue;
                if(p1Board[placement[0]+j][placement[1]+i].isShip())
                {
                    Graphics::putString("Invalid placement - Ship overlaps - try again\n");
                    return false;
                }
            }
        }
    }    

    return true;
}

// Checks is placement possible for Player2
bool Game::checkP2Placement(std::vector<int>& placement, bool vertical, Ship &ship)
{   // P2

    if(!checkIfInBoard(placement)) 
    {
        return false;
    } // Board limits

    // checks if ship fits
    if(vertical)
    {        
        if(placement[0]+ship.getLength()>10) // Check borders
        {
            return false;
        }
        for(int i = -1; i <= (int)ship.getLength(); i++) // Check overlap
        {
            for(int j = -1; j < 3; j++)
            {
                if(!checkIfInBoard(placement[0]+i, placement[1]+j)) continue;
                if(p2Board[placement[0]+i][placement[1]+j].isShip())
                {
                    return false;
                }
            }            
        }    
    }
    else
    {
        if(placement[1]+ship.getLength()>10) // Check borders
        {
            return false;
        }
        for(int i = -1; i <= (int)ship.getLength(); i++) // Check overlap
        {
           for(int j = -1; j < 2; j++)
            {
                if(!checkIfInBoard(placement[0]+j, placement[1]+i)) continue;
                if(p2Board[placement[0]+j][placement[1]+i].isShip())
                {
                    return false;
                }
            }
        }
    }        

    return true;
}

void Game::playerSetShip()
{   
    for(unsigned i = 0; i < p1Ships.size(); i++)
    {
        Graphics::clearBoard();
        Graphics::putString("Positioning the Ships: ");
        std::cout<<"Positioning ship of size: "<<p1Ships[i].getLength() << std::endl;
        Graphics::drawPlayerBoard(*this);

        bool vertical = true;
        bool shipSet = false;

        while(!shipSet)
        { 
            std::vector<int> position;
            bool vertical = true;
            Graphics::getInput(*this, i, position);
            
            if(p1Ships[i].getLength() > 1)
            {
                vertical = Graphics::yesNoChoice("Vertical or horizontal?", 'v','h');
            }

            if(!checkP1Placement(position,vertical,p1Ships[i]))
            {
                continue;
            }

            p1PlaceShip(position,vertical,p1Ships[i]);

            shipSet = true;
        }       
    }
}

void Game::loadPlacingFromFile(int player)
{
    std::fstream file;
    std::string fileName; 
    srand(time(NULL));

    fileName = "preset"+ std::to_string(rand()%numberOfAiPresets + 1)+".stp";

    std::string path = "../data/" + fileName;

    file.open(path, std::ios::in);
    if( file.good() != true )
    { 
        throw (std::string) "\x1b[31mFILE OPEN ERROR\x1b[0m: File '" + fileName + "' can not be opened";
    }
    int ship = 0;
    int mode = 0;

    while(!file.eof())
    {        
        std::string data;
        file >> data;
        if(data[0] == '#')
        {
            if(data == "#head") mode = 0;
            else if(data == "#ships") mode = 1;
            else break;
            continue;
        }

        if(mode == 0)
        {
            if(std::stoi(data) != shipSizes.size())
                throw (std::string) "FILE CONTAINS INCORRECT AMOUNT OF SHIPS!";
            
        }
        else if(mode == 1)
        {           
            std::string position = data;
            file >> data;
            bool vertical = (bool)std::stoi(data);
            std::vector<int> placement;
            Graphics::inputConv(position, placement);

            if(player == 0)
            {
                if(checkP1Placement(placement,vertical,p1Ships[ship]))
                {
                    p1PlaceShip(placement,vertical,p1Ships[ship]);
                    ship++;
                }
                else
                {
                    throw (std::string) "\x1b[31mFILE SHIP PLACEMENT ERROR\x1b[0m: file '" + fileName + "' contains invalid placement / placement invalid for given game configuration";
                }
            }
            else
            {
                if(checkP2Placement(placement,vertical,p2Ships[ship]))
                {
                    p2PlaceShip(placement,vertical,p2Ships[ship]);
                    ship++;
                }
                else
                {
                    throw (std::string) "\x1b[31mFILE SHIP PLACEMENT ERROR\x1b[0m: file '" + fileName + "' contains invalid placement / placement invalid for given game configuration";
                }
            }
        }
    }   

    if(ship != shipSizes.size())
    {
        throw (std::string) "\x1b[31mFILE SHIP PLACEMENT ERROR\x1b[0m: file '" + fileName + "' contains too litle / too much ship coordinates for current game configuration";
    }
}   

//Loads game configuration from config.cfg file
void Game::loadConfigFromFile()
{
    std::fstream config;
    config.open("../data/config.cfg", std::ios::in);

    if(!config.good())
    {
        throw (std::string) "\x1b[31mFILE OPEN ERRORx1b[0m: File 'config.cfg' can not be opened.";
    }
    
    int mode = 0;

    while(!config.eof())
    {
        std::string data;
        config >> data;        

        if(data[0] == '#')
        {
            if(data == "#ShipConfig")
                mode = 0; // shipSizes
            else if(data == "#numberOfAiPresets")
                mode = 1;            
        }
        else
        {
            if(mode == 0) // shipSizes
            {
                if(std::stoi(data) <= 0 || std::stoi(data) > 10)
                {
                    throw (std::string) "\x1b[31mCONFIG FILE ERROR\x1b[0m: All ships sizes must be grater than 0 and less than 11";
                }
                shipSizes.push_back(std::stoi(data));
            }
            else if(mode == 1)
            {
                numberOfAiPresets = std::stoi(data);
            }
                
        } 
    }
}

void Game::hitShip(std::vector<int>& placement, int player)
{
    if(player == 0) // player
    {
        p2Board[placement[0]][placement[1]].hitShip(); 
    }
    else // AI
    {
        p1Board[placement[0]][placement[1]].hitShip();
    }
}

