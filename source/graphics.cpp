
#include "../headers/graphics.h"

void Graphics::drawPlayerBoard(Game &curent_game)
{
    std::cout << "\nYour map:" << std::endl
              << " ";
    for (int i = 0; i < 10; i++)
    {
        std::cout.width(3);
        std::cout << (char)(65 + i);
    }
    std::cout << std::endl;
    for (int y = 0; y < 10; y++)
    {
        std::cout << std::setw(2) << y + 1;

        for (int x = 0; x < 10; x++)
        {
            std::cout.width(3);
            if (curent_game.p1Board[y][x].ship == nullptr)
            {
                if (curent_game.p1Board[y][x].hit == true)
                {
                    std::cout << "\x1b[44;30m x \x1b[0m";
                }
                else
                {
                    std::cout << "\x1b[44m ~ \x1b[0m";
                }
            }

            if (curent_game.p1Board[y][x].ship != nullptr)
            {
                if (curent_game.p1Board[y][x].hit == true)
                {
                    std::cout << "\x1b[41m X \x1b[0m";
                }
                else
                {
                    std::cout << "\x1b[42m O \x1b[0m";
                }
            }
        }
        std::cout << "|" << std::endl;
    }
}

void Graphics::drawEnemyBoard(Game &curent_game)
{
    std::cout << "\nProjection of enemy map:" << std::endl
              << " ";
    for (int i = 0; i < 10; i++)
    {
        std::cout.width(3);
        std::cout << (char)(65 + i);
    }
    std::cout << std::endl;
    for (int y = 0; y < 10; y++)
    {
        std::cout << std::setw(2);
        std::cout << y + 1;

        for (int x = 0; x < 10; x++)
        {
            std::cout.width(3);
            if (curent_game.p2Board[y][x].hit == true)
            {
                if (curent_game.p2Board[y][x].ship == nullptr)
                {
                    std::cout << "\x1b[44;30m x \x1b[0m";
                }
                else
                {
                    if (curent_game.p2Board[y][x].getShip()->isSunk())
                    {
                        std::cout << "\x1b[41m X \x1b[0m";
                    }
                    else
                    {
                        std::cout << "\x1b[43m X \x1b[0m";
                    }
                }
            }
            else
            {
                std::cout << "\x1b[44m ~ \x1b[0m";
            }
        }
        std::cout << "|" << std::endl;
    }
}

void Graphics::postGameDrawEnemyBoard(Game &curent_game)
{
    std::cout << "\nEnemy map:" << std::endl << " ";
    for (int i = 0; i < 10; i++)
    {
        std::cout.width(3);
        std::cout << (char)(65 + i);
    }
    std::cout << std::endl;
    for (int y = 0; y < 10; y++)
    {
        std::cout << std::setw(2) << y + 1;

        for (int x = 0; x < 10; x++)
        {
            std::cout.width(3);
            if (curent_game.p2Board[y][x].ship == nullptr)
            {
                if (curent_game.p2Board[y][x].hit == true)
                {
                    std::cout << "\x1b[44;30m x \x1b[0m";
                }
                else
                {
                    std::cout << "\x1b[44m ~ \x1b[0m";
                }
            }

            if (curent_game.p2Board[y][x].ship != nullptr)
            {
                if (curent_game.p2Board[y][x].hit == true)
                {
                    std::cout << "\x1b[41m X \x1b[0m";
                }
                else
                {
                    std::cout << "\x1b[43m O \x1b[0m";
                }
            }
        }
        std::cout << "|" << std::endl;
    }
}

void Graphics::clearBoard()
{
    std::cout << "\33c\e[3J" << std::endl;
}

void Graphics::putString(std::string text)
{
    std::cout << std::endl
              << text << std::endl;
}

void Graphics::getInput(Game &curent_game, int ship_numb, std::vector<int> &vec)
{
    std::cout << "Select field in range A1-J10: " << std::endl;
    std::string inputs;
    std::cin >> inputs;

    inputConv(inputs, vec);
}

void Graphics::inputConv(std::string input, std::vector<int> &vec)
{
    vec.resize(2);
    if (input[0] >= 65 && input[0] <= 90)
        vec[0] = input[0] - 65;
    else
        vec[0] = input[0] - 97;
    if (input.length() > 2)
    {
        vec[1] = 9;
    }
    else
    {
        vec[1] = input[1] - 49;
    }
    std::swap(vec[0], vec[1]);
}

bool Graphics::yesNoChoice(std::string question, char ans1, char ans2)
{
    char choice;
    if (ans1 >= 97 && ans1 <= 122)
        ans1 -= 32;
    if (ans2 >= 97 && ans2 <= 122)
        ans2 -= 32;

    while (true)
    {
        std::cout << question << " ( " << ans1 << "/" << ans2 << " )\n";
        std::cin >> choice;

        if (choice == ans1 || choice == ans1 + 32)
        {
            return true;
        }
        else if (choice == ans2 || choice == ans2 + 32)
        {
            return false;
        }
        else
        {
            std::cout << "Input incorrect! Try again:" << question << " ( " << ans1 << "/" << ans2 << ")\n";
        }
    }
}
