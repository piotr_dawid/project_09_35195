#include "../headers/ai.h"

Ai::Ai(std::vector<int> &sizes)
{
    targetShipSizes = sizes;
    shipsSunk.resize( targetShipSizes.size(), false);

    int min_size = sizes[0];
    for (int i = 0; i < sizes.size(); i++)
    {
        if (sizes[i] < min_size)
            min_size = sizes[i];
    }

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (min_size == 1)
            {
                target[i][j] = 0.5;
            }
            else
            {
                target[i][j] = 0.5 - (((i + j) % 2) / 100.0);
            }
        }
    }
    try
    {
        loadConfigFromFile();
    }
    catch(const std::string e)
    {
        std::cout << e << '\n';
        exit(EXIT_FAILURE);
    }    
    
    targetOptimize();
}

Ai::~Ai()
{

}

void Ai::printTarget()
{
    float maxTarget = 0;

    for (size_t i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
           if(target[i][j] > maxTarget) 
           maxTarget = target[i][j];
        }
    }

    for (size_t i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if(target[i][j] == maxTarget) std::cout << "\x1b[30;47m";
            else if(target[i][j] > maxTarget*0.7) std::cout << "\x1b[43m";
            else if(target[i][j] > maxTarget*0.5) std::cout << "\x1b[42m";
            else if(target[i][j] > maxTarget*0.2) std::cout << "\x1b[44m";
            else if(target[i][j] > 0) std::cout << "\x1b[45m";
            else std::cout << "\x1b[40m";
            std::cout.width(10);
            
            std::cout << target[i][j] << "|";
            std::cout.width(1);
            std::cout << "\x1b[0m";
        }
        std::cout << std::endl;
    }
}

void Ai::targetModify(int y_axis, int x_axis, bool hit, bool sunk)
{
    std::cout << "Pozycja [" <<  y_axis << "][" << x_axis << "], isHit:" << hit << " isSunk:" << sunk << std::endl;/*DEBUG*/
    if (hit == true)
    {
        // to know that a whole ship is taken down
        std::vector<int> hit = {y_axis, x_axis};
        last_ship.push_back(hit);

        // make neighbours more valuable
        for (int i = -1; i <= 1; i++)
        {
            if (Game::checkIfInBoard(y_axis + i, x_axis))
                target[y_axis + i][x_axis] *= hitMultiplier;
        }
        for (int j = -1; j <= 1; j++)
        {
            if (Game::checkIfInBoard(y_axis, x_axis + j))
                target[y_axis][x_axis + j] *= hitMultiplier;
        }

        if(last_ship.size()>1)
        {
            bool vertical = false;

            if(last_ship[0][0] == last_ship[1][0])
            {
                vertical = false;
            }
            else
            {
                vertical = true;
            }

            for(int i = 0; i < last_ship.size(); i++)
            {
                if(Game::checkIfInBoard(last_ship[i][0]-(!vertical),last_ship[i][1]-vertical))
                {
                    target[last_ship[i][0]-(!vertical)][last_ship[i][1]-vertical] *= missedMultiplier;
                }
                if(Game::checkIfInBoard(last_ship[i][0]+(!vertical),last_ship[i][1]+vertical))
                {
                    target[last_ship[i][0]+(!vertical)][last_ship[i][1]+vertical] *= missedMultiplier;
                }
            }
        }

        target[y_axis][x_axis] = 0;

        if (sunk == true)
        {

            for (int k = 0; k < last_ship.size(); k++)
            {
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        if (Game::checkIfInBoard(last_ship[k][0] + i, last_ship[k][1] + j))
                            target[last_ship[k][0] + i][last_ship[k][1] + j] *= missedMultiplier;
                    }
                }
            }
            lastSunkShip = last_ship.size();
            last_ship.clear();
            for(int i = 0; i < targetShipSizes.size(); i++)
            {
                if(lastSunkShip == targetShipSizes[i] && shipsSunk[i] == false)
                {
                    shipsSunk[i] = true;
                    break;
                }
            }
        }
    }
    else
    {
        target[y_axis][x_axis] = 0;
    }

    targetOptimize();
}

std::vector<int> Ai::returnCoOrdinates()
{
    std::vector<int>co_ordinates(2);
    float max_tar = target[0][0];
    int same_val_counter = 0;
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (target[i][j] == max_tar)
            {
                same_val_counter++;
            }
            else if (target[i][j] > max_tar)
            {
                same_val_counter = 1;
                max_tar = target[i][j];
            }
        }
    }

    srand(time(NULL));
    int selected_ship = (rand() % (same_val_counter))+1;
    int up_to_selected_ship = 0;

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (target[i][j] == max_tar)
            {
                up_to_selected_ship++;
            }
            if (up_to_selected_ship == selected_ship)
            {
                co_ordinates[0]=i;
                co_ordinates[1]=j;
                return co_ordinates;
            }
        }
    }
    return co_ordinates;
}

void Ai::targetOptimize()
{
    int targetShipSize = 0;

    // Select biggest unsunk Ship as next target
    for(int i = 0; i < targetShipSizes.size(); i++)
    {
        if(!shipsSunk[i] && targetShipSizes[i] > targetShipSize)
        {
            targetShipSize =  targetShipSizes[i];
        }
    }

    // Iterate for every field
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            // Check for vertical placement and horizontal
            for(int vertical = 0;  vertical <= 1;  vertical++)
            {
                // Check left and right/top and bottom
                for(int direction = -1; direction < 2; direction+=2)
                {
                    if(!Game::checkIfInBoard(i + ((targetShipSize-1)*vertical)*direction, j + ((targetShipSize-1)*(!vertical))*direction))
                    {
                        for(int size = 0; size < targetShipSize; size++)
                        {
                            if(!Game::checkIfInBoard(i + (size*vertical)*direction,j +  (size*(!vertical)*direction))) continue;
                            target[i + (size*vertical)*direction][j +  (size*(!vertical))*direction] *= placementImpossibleMultiplier;
                        }
                        break;
                    }
                    else
                    {
                        bool hasBeenHit = false;

                        for(int size = 0; size < targetShipSize; size++)
                        {
                            if(target[i + (size*vertical)*direction][j +  (size*(!vertical))*direction] == missedMultiplier)
                            {
                                hasBeenHit = true;
                                break;
                            }
                        }

                        if(!hasBeenHit)
                        {
                            for(int size = 0; size < targetShipSize; size++)
                            {
                                target[i + (size*vertical)*direction][j +  (size*(!vertical))*direction] *= placementPossibleMultiplier;
                            }
                        }
                        else
                        {
                            for(int size = 0; size < targetShipSize; size++)
                            {
                                target[i + (size*vertical)*direction][j +  (size*(!vertical))*direction] *= placementImpossibleMultiplier;
                            }
                        }
                    }
                }
            }
        }
    }
}

void Ai::loadConfigFromFile()
{
    std::fstream config;
    std::string path = "../data/ai_config.cfg";

    config.open(path, std::ios::in);

    if(!config.good())
    {
        throw (std::string) "\x1b[31mFILE OPEN ERROR\x1b[0m: File '" + path + "' can not be opened";
    }

    std::string data;
    int mode = 0;
    int mode0counter = 0;

    while(!config.eof())
    {
        config >> data;

        if(data[0] == '#')
        {
            if(data == "#aiMultipliers") 
            {
                mode = 0;
                continue;
            }
            else if(data == "#end") break;
            else continue;
        }

        if(mode == 0)
        {
            if(mode0counter == 0)
            {
                hitMultiplier = std::stof(data);
                mode0counter++;
            }
            else if(mode0counter == 1)
            {
                placementPossibleMultiplier = std::stof(data);
                mode0counter++;
            }
            else if(mode0counter == 2)
             {
                placementImpossibleMultiplier = std::stof(data);
                mode0counter++;
            }
        }
    }   
}