#ifndef GRAPHICS
#define GRAPHICS

#include <iostream>
#include <iomanip> 
#include <string>
#include <vector>

#include "game.h"

class Graphics
{
    public:
     //const static std::string no_hit_water='~';
     //const static char hit_water='x';
     //const static char no_hit_ship='O';
    // const static char hit_ship='X';


    static void drawPlayerBoard(Game &);
    static void drawEnemyBoard(Game &);
    static void postGameDrawEnemyBoard(Game &);
    static void clearBoard();
    static void putString(std::string);
    static void getInput(Game &,int,std::vector<int>& );
    static void inputConv(std::string,std::vector<int>&);
    static bool yesNoChoice(std::string question, char ans1, char ans2);


};



#endif // GRAPHICS
