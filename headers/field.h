#ifndef FIELD
#define FIELD

#include "ship.h" 

class Field
{
    public:
        Field();
        
        void setShip(Ship* newShip);
        bool isHit();
        Ship* getShip();
        void hitShip();    
        bool isShip();

    private:
        Ship* ship;
        bool hit;
        
        friend class Graphics;

        //do edycji

};

#endif //FIELD