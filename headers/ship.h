#ifndef ship_h
#define ship_h

class Ship
{
private:
    unsigned ship_length;
    unsigned hp;

public:
    bool isSunk();
    void hit();
    unsigned getLength();
    Ship(unsigned);
    ~Ship();
};

#endif //
