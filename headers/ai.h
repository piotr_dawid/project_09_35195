#ifndef AI
#define AI
#include "game.h"
#include <ctime>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <string>

class Ai
{
public:
    Ai(std::vector<int> &sizes);
    ~Ai();
    std::vector<int> returnCoOrdinates();
    void printTarget();
    void targetModify(int,int,bool,bool);

private:
    void loadConfigFromFile(); 
    void targetOptimize();

    float target[10][10] = {0};
    std::vector<std::vector<int>> last_ship;     
    std::vector<int> targetShipSizes;
    std::vector<bool> shipsSunk;
    int lastSunkShip;
    const float missedMultiplier = 0;
    float hitMultiplier = 11;
    float placementPossibleMultiplier = 1.03;
    float placementImpossibleMultiplier = 0.99;    
};


#endif // AI
