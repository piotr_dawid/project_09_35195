#ifndef GAME
#define GAME

#include "field.h"
#include "ship.h"
#include "ai.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <stdlib.h> 

// Main Game class - offers core game functionality
class Game
{
    friend class Graphics; 
    public:
        Game();
        // Game moments
        void start(); 
        void result();

        static bool checkIfInBoard(int y, int x);

    private:   
        void playerSetShip();
        void aiSetShips(); 
        void hitShip(std::vector<int>& placement, int player);
        bool checkP1Placement(std::vector<int>& placement, bool vertical, Ship &ship);
        bool checkP2Placement(std::vector<int>& placement, bool vertical, Ship &ship);
        bool checkIfInBoard(std::vector<int>& placement);
        
        void p1PlaceShip(std::vector<int>& placement, bool vertical, Ship &ship);
        void p2PlaceShip(std::vector<int>& placement, bool vertical, Ship &ship);
        void loadPlacingFromFile(int player);
        void loadConfigFromFile();
   
        Field p1Board[10][10];
        Field p2Board[10][10];
        std::vector<Ship> p1Ships;
        std::vector<Ship> p2Ships;
        int p1ShipNumber;
        int p2ShipNumber;
        bool gameStatus;
              
        unsigned turn; // Is it config or not? now IDK
        
        //Game Config
        std::vector<int> shipSizes;   
        int numberOfAiPresets;
};

#include "graphics.h"

#endif //GAME